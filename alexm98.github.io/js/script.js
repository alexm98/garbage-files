$(document).ready(function(){
    // description on main page
    var text = ["Web Development Passionate", "Volunteer", "CoderDojo Volunteer","Robotics Fan","Linux User"];
    var index = 0;
    var description = $("#description");

    description.fadeOut("slow", changeText);

    function changeText(){
        description.html(text[index]).fadeIn("slow", function() {

            description.delay(1000).fadeOut("slow", function() {
                index++;
                if (index == text.length) {
                    index = 0;
                };
                setTimeout(changeText, 400);
            });
        });
    }

    var up = function(){
      $("#down").animate({"top" : "-=20px"},800,"linear",down);
    }
    var down = function(){
      $("#down").animate({"top" : "+=20px"},800,"linear",up);
    }
   
    $(document).ready(function(){
        up();

        $("#firstproject").hover(function(){
            $("#firstprojectinfo").stop().slideToggle(300);
        });

        $("#secondproject").hover(function(){
            $("#secondprojectinfo").stop().slideToggle(300);
        });

        $("#thirdproject").hover(function(){
            $("#thirdprojectinfo").stop().slideToggle(300);
        });

        $("#fourthproject").hover(function(){
            $("#fourthprojectinfo").stop().slideToggle(300);
        });

    });

    //clicking on the down hovering button to scroll 
    $('#down').click(function(){
    	$('html, body').animate({scrollTop: $("#skillspage").offset().top}, 1000);
    });

    //nav scrolling 
    $('#projects').click(function(){
    	$('html, body').animate({scrollTop: $("#skillspage").offset().top}, 1000);
	});
    $('#skills').click(function(){
    	$('html, body').animate({scrollTop: $("#projectpage").offset().top}, 1000);
	});
    $('#about').click(function(){
    	$('html, body').animate({scrollTop: $("#aboutpage").offset().top}, 1000);
	});
});
