$(document).ready(function(){

    var text = ["Web Development Passionate", "Volunteer", "CoderDojo Oradea Organizer","Robotics Fan","Linux User"];
    var index = 0;
    var description = $("#description");

    description.fadeOut("slow", changeText);

    function changeText(){
        description.html(text[index]).fadeIn("slow", function() {

            description.delay(1000).fadeOut("slow", function() {
                index++;
                if (index == text.length) {
                    index = 0;
                };
                setTimeout(changeText, 400);
            });
        });
    }

});